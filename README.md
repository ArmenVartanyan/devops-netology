# devops-netology
Проигнорированы будут следующие файлы.

всё в локальном каталоге .terraform
**/.terraform

Проигнорируются файлы с расширением:
*.tfstate

Проигнорируется файл:
crash.log

Проигнорируются файлы с расширением:
* .tfvars

Проигнорируются файлы:
override.tf

override.tf.json

Проигнорируются файлы:
*_override.tf
*_override.tf.json

Проигнорируются файлы с расширением:
.terraformrc


Проигнорируется файл:
terraform.rc
